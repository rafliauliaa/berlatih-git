<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  <link rel="icon" href="https://sanbercode.com/assets_new/images/logo/logo-reduced.png">
  <title>Sanbercode - Taks 06 - Belajar Git</title>
</head>

<body>
  <h3><span class="badge badge-secondary">My Bio</span></h3>
  <hr>
  <table class="table">
    <tr>
      <th scope="row">Nama</th>
      <td>:</td>
      <td>Rafli A</td>
    </tr>
    <tr>
      <th scope="row">Domisili</th>
      <td>:</td>
      <td>Bandung, Jawa Barat</td>
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td>:</td>
      <td>rafliauliaa@gmail.com</td>
    </tr>
    </tbody>
  </table>
</body>

</html>